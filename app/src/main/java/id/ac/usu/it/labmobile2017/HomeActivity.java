package id.ac.usu.it.labmobile2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.buttonOK)
    Button buttonOK;

    @BindView(R.id.editTextNama)
    EditText editTextNama;

    Toolbar toolbarHome;
    Button buttonNext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        toolbarHome = (Toolbar) findViewById(R.id.toolbarHome);
        buttonNext = (Button) findViewById(R.id.buttonNext);

        setSupportActionBar(toolbarHome);
        setTitle(R.string.nama_activity_satu);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = editTextNama.getText().toString();

                //potong spasi
                nama = nama.trim();

                if (nama.isEmpty()) {
                    String errorMessage = HomeActivity.this.getResources().getString(R.string.error_nama_kosong);

                    editTextNama.setError(errorMessage);
                    return;
                }

                //nama siap dikirim ke activity kedua
                Intent intent = new Intent(HomeActivity.this, SecondActivity.class);
                intent.putExtra("key_nama", nama);


                HomeActivity.this.startActivity(intent);
            }
        });

    }

    @OnClick(R.id.buttonOK)
    public void sesuatu() {
        String namaYangDiinput = editTextNama.getText().toString();

        String hasil = namaYangDiinput + " Ganteng";

        Toast.makeText(HomeActivity.this, hasil, Toast.LENGTH_SHORT).show();
    }
}
