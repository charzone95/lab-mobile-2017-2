package id.ac.usu.it.labmobile2017;

import android.os.Environment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.usu.it.labmobile2017.adapters.MahasiswaAdapter;
import id.ac.usu.it.labmobile2017.models.Mahasiswa;
import id.ac.usu.it.labmobile2017.response.MahasiswaResponse;
import id.ac.usu.it.labmobile2017.service.MahasiswaService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MahasiswaActivity extends AppCompatActivity {

    @BindView(R.id.recyclerViewMahasiswa)
    RecyclerView recyclerViewMahasiswa;

    @BindView(R.id.swipeRefreshMahasiswa)
    SwipeRefreshLayout swipeRefreshMahasiswa;

    @BindView(R.id.toolbarMahasiswa)
    Toolbar toolbarMahasiswa;

    @BindColor(R.color.swipeColor1)
    int swipeColor1;

    @BindColor(R.color.swipeColor2)
    int swipeColor2;

    @BindColor(R.color.swipeColor3)
    int swipeColor3;


    List<Mahasiswa> listData;
    MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa);
        ButterKnife.bind(this);

        this.setSupportActionBar(toolbarMahasiswa);
        this.setTitle("Daftar Mahasiswa");

        swipeRefreshMahasiswa.setColorSchemeColors(swipeColor1, swipeColor2, swipeColor3);

        swipeRefreshMahasiswa.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               loadData();
            }
        });

        listData = new ArrayList<>();
        adapter = new MahasiswaAdapter(listData);
        recyclerViewMahasiswa.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewMahasiswa.setLayoutManager(layoutManager);

        //tambah garis pembatas
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        recyclerViewMahasiswa.addItemDecoration(itemDecoration);


        loadData();
    }

    private void loadData() {
        if (!swipeRefreshMahasiswa.isRefreshing()) {
            swipeRefreshMahasiswa.setRefreshing(true);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MahasiswaService.baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        MahasiswaService service = retrofit.create(MahasiswaService.class);

        service.listMahasiswa().enqueue(new Callback<List<MahasiswaResponse>>() {
            @Override
            public void onResponse(Call<List<MahasiswaResponse>> call, Response<List<MahasiswaResponse>> response) {
                List<MahasiswaResponse> hasil = response.body();

                listData.clear();
                for (MahasiswaResponse mahasiswa : hasil) {
                    listData.add(new Mahasiswa(mahasiswa.getNama(), mahasiswa.getNim(), mahasiswa.getPhoto()));
                }
                adapter.notifyDataSetChanged();

                swipeRefreshMahasiswa.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<MahasiswaResponse>> call, Throwable t) {
                Toast.makeText(MahasiswaActivity.this, "Gagal Load Data", Toast.LENGTH_SHORT).show();

                swipeRefreshMahasiswa.setRefreshing(false);
            }
        });

    }
}
