package id.ac.usu.it.labmobile2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {

    @BindView(R.id.imageViewCoba)
    ImageView imageViewCoba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);

        GlideApp.with(this)
                .load("https://3.bp.blogspot.com/-K1LhVZUi4qw/VxiJ55YEthI/AAAAAAAAQeg/4ayCo7iRKrU_XyYZU61c-w-jHTAQkJ0LQCLcB/s1600/twice_nayeon5.jpg")
                .placeholder(R.drawable.twice_logo)
                .centerCrop()
                .into(imageViewCoba);
    }
}
