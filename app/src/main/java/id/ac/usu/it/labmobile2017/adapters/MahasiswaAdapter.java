package id.ac.usu.it.labmobile2017.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import id.ac.usu.it.labmobile2017.GlideApp;
import id.ac.usu.it.labmobile2017.R;
import id.ac.usu.it.labmobile2017.models.Mahasiswa;
import id.ac.usu.it.labmobile2017.viewholders.MahasiswaViewHolder;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaViewHolder> {
    private List<Mahasiswa> listData;

    public MahasiswaAdapter(List<Mahasiswa> listData) {
        this.listData = listData;
    }


    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_mahasiswa, parent, false);

        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaViewHolder holder, int position) {
        Mahasiswa data = listData.get(position);

        holder.textViewNama.setText(data.getNama());
        holder.textViewNim.setText(data.getNim());

        //load foto pakai glide
        GlideApp.with(holder.rootView.getContext())
                .load(data.getPhoto())
                .placeholder(R.drawable.twice_logo)
                .into(holder.imageViewFoto);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
