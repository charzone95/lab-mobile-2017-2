package id.ac.usu.it.labmobile2017.service;

import java.util.List;

import id.ac.usu.it.labmobile2017.response.MahasiswaResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MahasiswaService {
    public String baseUrl = "https://mobile.charzone95.web.id/api-mobile-2017/";

    @GET("list.php")
    Call<     List<MahasiswaResponse>    > listMahasiswa();

}
