package id.ac.usu.it.labmobile2017.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.usu.it.labmobile2017.R;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rootView)
    public View rootView;

    @BindView(R.id.imageViewFoto)
    public ImageView imageViewFoto;

    @BindView(R.id.textViewNama)
    public TextView textViewNama;

    @BindView(R.id.textViewNim)
    public TextView textViewNim;

    public MahasiswaViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
